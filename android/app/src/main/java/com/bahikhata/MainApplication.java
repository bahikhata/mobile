package com.bahikhata;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import io.realm.react.RealmReactPackage;
import java.util.Arrays;
import java.util.List;
import com.reactnativenavigation.NavigationApplication;
import com.bahikhata.calculator.CalculatorPackage;
import com.rhaker.reactnativeselectcontacts.ReactNativeSelectContacts;
import com.imagepicker.ImagePickerPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.reactnativenavigation.NavigationApplication;

public class MainApplication extends NavigationApplication{

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    @Override
    public String getJSBundleFile() {
        return CodePush.getJSBundleFile();
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
      return Arrays.<ReactPackage>asList(
              new RealmReactPackage(),
              new VectorIconsPackage(),
              new CalculatorPackage(),
              new ReactNativeSelectContacts(),
              new ImagePickerPackage(),
              new ReactNativeRestartPackage(),
              new RNI18nPackage()
      );
    }
}
