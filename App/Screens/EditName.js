import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, StyleSheet, View, Text, Dimensions, Button, TextInput } from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const _ = require('lodash');
const realm = require('../Services/dbClient');
const validate = require('validate.js');
const I18n = require('../I18n/I18n');

const constraints = {
  name: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class EditName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      nameError: ''
    };
    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });
    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
  }

  componentWillMount() {
    const user = realm.objects('User');
    this.setState({
      name: user[0].name
    });
  }

  componentWillUnmount() {
    this.onPressButtonDebounced.cancel();
  }

  onPressButton = () => {
    const error = validate({ name: this.state.name }, constraints);
    if (!error) {
      realm.write(() => {
        realm.create('User', {
          id: 1,
          name: this.state.name,
          updated: new Date()
        }, true);
      });
      global.navigator.pop({
        animated: true,
        animationType: 'slide-horizontal'
      });
    } else {
      this.setState({
        nameError: error.name[0]
      });
    }
  }
  render() {
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
        <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('business')}
              value={this.state.name}
              keyboardType="default"
              maxLength={25}
              autoFocus
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ name: text, nameError: '' })}
        />
          {this.state.nameError ?
            <Text style={ApplicationStyles.inputError}>{this.state.nameError}</Text> :
            null}
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
