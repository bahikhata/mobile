import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, Image, TextInput,
  ScrollView, Button, TouchableHighlight,
  Platform, ToastAndroid, DatePickerAndroid, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Calculator from 'react-native-calculator';
import moment from 'moment';
import SelectContacts from 'react-native-select-contact-android';
import ImagePicker from 'react-native-image-picker';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';
import Autocomplete from '../Components/Autocomplete';

const _ = require('lodash');
const realm = require('../Services/dbClient');
const I18n = require('../I18n/I18n');
const validate = require('validate.js');

const constraints = {
  amount: {
    presence: {
      message: I18n.t('required')
    },
    format: {
      pattern: /^[0-9]+$/,
      message: I18n.t('invalidNo')
    }
  }
};
const options = {
  title: I18n.t('attach'),
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

export default class New extends Component {
  constructor(props) {
    super(props);

    this.state = {
      amount: '',
      amountError: '',
      detail: '',
      account: '',
      phone: '',
      date: new Date(),
      attachment: '',
      data: realm.objects('Account').map(account => account.name),
      hideResults: false
    };
    
    this.onTxnDoneDebounced = _.debounce(this.onTxnDone, 300);
    this.pickContactDebounced = _.debounce(this.pickContact, 300);
    this.pickAttachmentDebounced = _.debounce(this.pickAttachment, 300);
    this.showPickerDebounced = _.debounce(this.showPicker, 300);
  }

  componentWillMount() {
    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });

    this.props.navigator.setButtons({
      rightButtons: [{
        title: moment(this.state.date).format('DD MMM'),
        id: 'datePick',
        buttonColor: Colors.snow
      }],
      animated: false
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  componentWillUnmount() {
    this.onTxnDoneDebounced.cancel();
    this.pickContactDebounced.cancel();
    this.pickAttachmentDebounced.cancel();
    this.showPickerDebounced.cancel();
  }

  onNavigatorEvent = (event) => {
    if (event.id === 'datePick') {
      this.showPickerDebounced({ date: this.state.date, maxDate: new Date() });
    }
  }

  onTxnDone = (multiplier) => {
    //
    const error = validate({ amount: this.state.amount }, constraints);

    if (!error) {
      //Account entered for transaction.
      if (this.state.account.length > 0) {
        //Check if account exists ?
        const account = realm.objects('Account').filtered(`name = "${this.state.account}"`);
        //Account does not exist. Create a new account.
        if (account.length === 0) {
          realm.write(() => {
              realm.create('Account', {
                id: (realm.objects('Account').length > 0 ?
                  realm.objects('Account').max('id') : 0) + 1,
                name: this.state.account,
                amount: this.state.amount * multiplier,
                phone: this.state.phone
              });

              const newAccount = realm.objects('Account')
                .filtered(`name = "${this.state.account}"`);
              realm.create('Transaction', {
                id: (realm.objects('Transaction').length > 0 ?
                  realm.objects('Transaction').max('id') : 0) + 1,
                account: newAccount[0],
                amount: this.state.amount * multiplier,
                detail: this.state.detail,
                attach: this.state.attachment === '' ? '' : this.state.attachment.uri,
                updated: this.state.date,
                created: this.state.date
              });
          });
        } else {
          realm.write(() => {
            realm.create('Transaction', {
              id: (realm.objects('Transaction').length > 0 ?
                realm.objects('Transaction').max('id') : 0) + 1,
              account: account[0],
              amount: this.state.amount * multiplier,
              detail: this.state.detail,
              attach: this.state.attachment === '' ? '' : this.state.attachment.uri,
              updated: this.state.date,
              created: this.state.date
            });
            realm.create('Account', {
              id: account[0].id,
              amount: account[0].amount + (this.state.amount * multiplier),
              updated: new Date()
            }, true);
          });
        }
      } else {
        realm.write(() => {
          realm.create('Transaction', {
            id: (realm.objects('Transaction').length > 0 ?
              realm.objects('Transaction').max('id') : 0) + 1,
            account: null,
            amount: this.state.amount * multiplier,
            detail: this.state.detail,
            attach: this.state.attachment === '' ? '' : this.state.attachment.uri,
            updated: this.state.date,
            created: this.state.date
          });
        });
      }
      this.props.navigator.pop({
        animated: true,
        animationType: 'slide-horizontal'
      });
    } else {
      this.setState({
        amountError: error.amount[0]
      });
    }
  }

  showPicker = async(options) => {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open(options);
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        this.setState({
          date: new Date(year, month, day)
        });
        //this.updateTxns();
        this.props.navigator.setButtons({
          rightButtons: [{
            title: moment(this.state.date).format('DD MMM'),
            id: 'datePick',
            buttonColor: Colors.snow
          }],
          animated: false
        });
      }
    } catch ({ code, message }) {
      console.warn('Error in example ', message);
    }
  };

  openCalculator = () => {
    if (Platform.OS === 'android') {
        Calculator.open();
    }
  }

  pickContact = () => {
    SelectContacts.pickContact({ timeout: 45000 }, (err, contact) => {
      if (err) {
        console.log(err.message);
      } else {
        //Check if contact does not exist of same name.
        const account = realm.objects('Account').filtered('name = $0', contact.name);
        if (account.length === 0) {
          this.setState({
            account: contact.name,
            phone: contact.phoneNumbers ? contact.phoneNumbers[0].number : ''
          });
        } else {
          ToastAndroid.show('Account already exists', ToastAndroid.SHORT);
        }
      }
    });
  }

  pickAttachment = () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };
        this.setState({
          attachment: source
        });
      }
    });
  }

  filterData = (query) => {
    if (query === '') {
      return [];
    }
    const data = this.state.data;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return data.filter(dat => dat.search(regex) >= 0);
  }

  render() {
    const data = this.filterData(this.state.account);
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
            <View style={styles.form}>
              <View style={styles.formRow}>
                <View style={styles.subRow}>
                  <TextInput
                      style={ApplicationStyles.inputControl}
                      placeholder={I18n.t('amount')}
                      keyboardType='numeric'
                      spellCheck={false}
                      autoCorrect={false}
                      maxLength={9}
                      ref='amount'
                      text={this.state.amount}
                      autoFocus
                      selectionColor={Colors.black}
                      placeholderTextColor={Colors.charcoal}
                      underlineColorAndroid={Colors.transparent}
                      onChangeText={(text) => {
                        this.setState({ amount: text, amountError: '' });
                      }}
                  />
                  <TouchableHighlight
                    underlayColor={Colors.underlayColor}
                    style={styles.calculatorIcon}
                  >
                    <Icon
                        name='calculator'
                        size={Metrics.icons.small}
                        color={Colors.black}
                        onPress={this.openCalculator}
                    />
                  </TouchableHighlight>
                </View>
                {
                  this.state.amountError ?
                    <Text style={ApplicationStyles.inputError}>{this.state.amountError}</Text>
                    : null
                }
                <TextInput
                    style={ApplicationStyles.inputControl}
                    placeholder={I18n.t('detail')}
                    multiline
                    autoCorrect={false}
                    maxLength={60}
                    value={this.state.detail}
                    selectionColor={Colors.black}
                    placeholderTextColor={Colors.charcoal}
                    underlineColorAndroid={Colors.transparent}
                    onChangeText={(text) => this.setState({ detail: text })}
                />
              </View>
              <TouchableHighlight
                underlayColor={Colors.underlayColor}
                style={styles.camera}
                onPress={this.pickAttachmentDebounced}
              >
                {
                  this.state.attachment === '' ?
                    <Icon
                        name='camera'
                        size={Metrics.icons.medium}
                        color={Colors.black}
                    /> :
                    <Image
                        style={styles.attachment}
                        source={this.state.attachment}
                    />
                }
              </TouchableHighlight>
            </View>
            <View style={styles.subRow}>
              <Autocomplete
                  style={ApplicationStyles.inputControl}
                  data={data}
                  placeholder={I18n.t('account')}
                  maxLength={35}
                  hideResults={this.state.hideResults}
                  autoCorrect={false}
                  spellCheck={false}
                  defaultValue={this.state.account}
                  selectionColor={Colors.black}
                  placeholderTextColor={Colors.charcoal}
                  underlineColorAndroid={Colors.transparent}
                  onChangeText={text => this.setState({ account: text, hideResults: false })}
                  renderItem={value => (
                    <TouchableOpacity
                      underlayColor={Colors.underlayColor}
                      style={styles.dropdown}
                      onPress={() => this.setState({ account: value, hideResults: true })}
                    >
                      <Text>{value}</Text>
                    </TouchableOpacity>
                  )}
              />
              <TouchableHighlight
                  underlayColor={Colors.underlayColor}
                  style={ApplicationStyles.contactIcon}
                  onPress={this.pickContactDebounced}
              >
                <Icon
                  name='user'
                  size={Metrics.icons.medium}
                  color={Colors.black}
                />
              </TouchableHighlight>
            </View>
            <View style={styles.buttonGroup}>
              <View style={styles.inButton}>
                <Button
                  disabled={!this.state.amount}
                  title={I18n.t('income')}
                  color={Colors.darkPrimary}
                  onPress={() => this.onTxnDoneDebounced(1)}
                />
              </View>
              <View style={styles.outButton}>
                <Button
                  disabled={!this.state.amount}
                  title={I18n.t('expense')}
                  color={Colors.darkPrimary}
                  onPress={() => this.onTxnDoneDebounced(-1)}
                />
              </View>
            </View>
        </ScrollView>
    );
  }
}

let styles = StyleSheet.create({
  camera: {
    flex: 0.25,
    padding: Metrics.smallMargin,
    marginRight: Metrics.smallMargin,
    marginTop: Metrics.doubleBaseMargin,
    width: Metrics.images.placeholder + 4,
    height: Metrics.images.placeholder + 4,
    borderColor: Colors.primary,
    borderWidth: 0.5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  attachment: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'stretch'
  },
  calculatorIcon: {
    flex: 0.1,
    marginTop: 1.5 * Metrics.baseMargin,
    marginRight: Metrics.baseMargin
  },
  attachImage: {
    margin: Metrics.doubleSection
  },
  inButton: {
    flex: 0.5,
    marginRight: Metrics.smallMargin,
    marginLeft: Metrics.baseMargin
  },
  outButton: {
    flex: 0.5,
    marginLeft: Metrics.smallMargin5,
    marginRight: Metrics.baseMargin
  },
  buttonGroup: {
    flex: 0.2,
    flexDirection: 'row',
    marginTop: Metrics.doubleBaseMargin
  },
  dropdown: {
    padding: Metrics.baseMargin
  },
  subRow: {
    flex: 0.8,
    flexDirection: 'row'
  },
  form: {
    flex: 0.5,
    flexDirection: 'row'
  },
  formRow: {
    flex: 0.7,
    flexDirection: 'column'
  }
});
