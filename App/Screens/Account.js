import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  Image,
  TouchableHighlight,
  Alert,
  FlatList,
  ToastAndroid,
  StyleSheet
} from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const _ = require('lodash');
const accounting = require('accounting');
const realm = require('../Services/dbClient');
const I18n = require('../I18n/I18n');

export default class Account extends Component {
  static navigatorStyle = {
    contextualMenuStatusBarColor: Colors.primary,
    contextualMenuBackgroundColor: Colors.primary,
    contextualMenuButtonsColor: Colors.snow
  };
  constructor(props) {
    super(props);
    /*this.props.navigator.toggleNavBar({
      to: 'hidden', // required, 'hidden' = hide navigation bar, 'shown' = show navigation bar
      animated: false // does the toggle have transition animation or does it happen immediately (optional). By default animated: true
    });*/

    this.state = {
        selected: -1
    };

    global.navigator = this.props.navigator;

    iconsLoaded.then(() => {
      this.props.navigator.setButtons({
        leftButtons: [{
          id: 'sideMenu',
          buttonColor: Colors.snow
        }],
        rightButtons: [{
          icon: iconsMap['user-plus'],
          id: 'addAccount',
          buttonColor: Colors.snow
        }],
        animated: false
      });
    });

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    this.addAccountDebounced = _.debounce(this.addAccount, 300);
    this.navigateDebounced = _.debounce(this.navigate, 300);
  }

  componentWillMount() {

  }

  componentWillUnmount() {
    this.addAccountDebounced.cancel();
    this.navigateDebounced.cancel();
    this.navigateDebounced.cancel();
  }

  onNavigatorEvent = (event) => {
    if (event.id === 'willAppear') {
      this.forceUpdate();
    } else if (event.id === 'bottomTabSelected' || event.id === 'bottomTabReselected') {
      this.props.navigator.dismissContextualMenu();
    } else if (event.id === 'addAccount') {
      this.addAccountDebounced();
    } else if (event.id === 'contextualMenuDismissed') {
      this.setState({
        selected: -1
      });
    }
  }

  addAcc = () => {
    
  }

  addAccount = () => {
    this.props.navigator.push({
      screen: 'screen.addAccount', // unique ID registered with Navigation.registerScreen
      title: I18n.t('addAccount'), // navigation bar title of the pushed screen (optional)
      passProps: {
        done: this.addAcc
      }, // Object that will be passed as props to the pushed screen (optional)
      animated: true,
      animationType: 'slide-horizontal',
      backButtonHidden: false, // hide the back button altogether (optional)
    });
  }

  toggleDrawer = () => {
    this.props.navigator.toggleDrawer({
      side: 'left',
      animated: true
    });
  };

  navigate = (rowId) => {
    //this.props.navigator.dismissContextualMenu();
    if (this.state.selected < 0) {
      this.props.navigator.push({
        screen: 'screen.details', // unique ID registered with Navigation.registerScreen
        title: '',
        passProps: {
          id: rowId
        }, // Object that will be passed as props to the pushed screen (optional)
        animated: true,
        animationType: 'slide-horizontal',
        backButtonHidden: false, // hide the back button altogether (optional)
      });
    }
  }

  longPress = (rowId) => {
    this.setState({
      selected: rowId
    });
    this.props.navigator.showContextualMenu(
      {
        rightButtons: [{
          title: I18n.t('close'),
          icon: iconsMap.trash
        },
        {
          title: I18n.t('rename'),
          icon: iconsMap.pencil
        }],
        onButtonPressed: (index) => {
          if (index === 0) {
            Alert.alert(
              '',
              I18n.t('closeAccMsg'),
              [
                { text: I18n.t('cancel') },
                { text: I18n.t('ok'), onPress: () => this.close(rowId) }
              ],
              { cancelable: true }
            );
          } else if (index === 1) {
            this.rename(rowId);
          }
        }
      }
    );
  }

  rename = (id) => {
    this.props.navigator.push({
      screen: 'screen.editAccount',
      title: I18n.t('editAcc'),
      passProps: {
        id
      },
      animated: true,
      animationType: 'slide-horizontal',
      backButtonHidden: false,
    });
  }

  close = (id) => {
    const account = realm.objects('Account').filtered('id = $0', id);
    if (account[0].amount === 0) {
      realm.write(() => {
        realm.delete(account);
      });
    } else {
      ToastAndroid.show(I18n.t('activeAccError'), ToastAndroid.SHORT);
    }

    this.forceUpdate();
  }

  renderSeparator = (index) => (
      <View key={index} style={ApplicationStyles.separator} />
  )

  renderItem = (rowData) => (
      <TouchableHighlight
        underlayColor={Colors.underlayColor}
        key={rowData}
        onPress={() => this.navigateDebounced(rowData.item.id)}
        onLongPress={() => this.longPress(rowData.item.id)}
      >
        <View
          style={[ApplicationStyles.rowAccount,
            this.state.selected === rowData.item.id ?
            { backgroundColor: Colors.lightPrimary } :
            { backgroundColor: 'transparent' }]}
        >
          <Text style={ApplicationStyles.account}>{rowData.item.name}</Text>
          <Text style={ApplicationStyles.amount}>
            <Text
              style={
                rowData.item.amount >= 0 ?
                { color: Colors.black } : { color: Colors.fire }
              }
            >
              {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
            </Text>
          </Text>
        </View>
      </TouchableHighlight>
  )

  render() {
    return (
      <Image style={styles.container} source={Images.background}>
        <FlatList
            style={{ flex: 0.9 }}
            data={realm.objects('Account').sorted('name', false)}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            ListFooterComponent={() => (
              realm.objects('Account').length > 0 ?
                <View style={ApplicationStyles.separator} />
                : null
            )}
        />
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    resizeMode: 'stretch'
  }
});
