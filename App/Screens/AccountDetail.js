import React, { Component } from 'react';
import PropTyps from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableHighlight,
  Linking
} from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import moment from 'moment';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const I18n = require('../I18n/I18n');
const realm = require('../Services/dbClient');
const accounting = require('accounting');

let account;

export default class AccountDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      balance: 0,
      txns: []
    };
  }
  componentWillMount() {
    account = realm.objects('Account').filtered('id= $0', this.props.id);

    this.props.navigator.setTitle({
      title: account[0].name.substr(0, 25)
    });
    this.setState({
      balance: account[0].amount
    });

    if (account[0].phone !== '') {
      iconsLoaded.then(() => {
        this.props.navigator.setButtons({
          rightButtons: [{
            icon: iconsMap.phone,
            id: 'call',
            buttonColor: Colors.snow
          }],
          animated: false
        });
      });

      this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });

    this.updateTxns();
  }
  onNavigatorEvent = (event) => {
    if (event.id === 'call') {
      Linking.openURL(`tel:${account[0].phone}`);
    }
    console.log(event.id);
  }
  updateTxns = () => {
      this.setState({
        txns: realm.objects('Transaction').filtered(`account.id = "${this.props.id}"`)
      });
  }
  viewAttachDetail = (rowId) => {
    const txn = realm.objects('Transaction').filtered('id = $0', rowId);
    //alert(txn[0].attach);

    this.props.navigator.push({
      screen: 'screen.attachDetail',
      passProps: {
        txn: txn[0]
      },
      animated: true,
      animationType: 'fade',
      backButtonHidden: false,
      navigatorStyle: {
        navBarBackgroundColor: Colors.black,
        navBarTextColor: Colors.snow,
        statusBarColor: Colors.black
      }
    });

    //
  }
  renderItem = (rowData) => (
      <TouchableHighlight underlayColor={Colors.underlayColor} key={rowData.index}>
        <View style={ApplicationStyles.row}>
            <Text style={ApplicationStyles.txnAmount}>
              {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
            </Text>
            <View style={ApplicationStyles.subrow}>
                <Text style={ApplicationStyles.detail}>{rowData.item.detail}</Text>
                { rowData.item.attach === '' ?
                  null :
                  (<TouchableHighlight
                    underlayColor={Colors.underlayColor}
                    onPress={() => this.viewAttachDetail(rowData.item.id)}
                  >
                    <Image
                      style={ApplicationStyles.attach} source={{ uri: rowData.item.attach }}
                    />
                  </TouchableHighlight>)
                }
                <Text style={ApplicationStyles.dateRow}>
                  {moment(rowData.item.created).format('DD MMM')}
                </Text>
            </View>
        </View>
    </TouchableHighlight>
  )
  renderSeparator = () => (
    <View style={ApplicationStyles.separator} />
  )
  render() {
    return (
      <Image style={styles.container} source={Images.background} resizeMode='stretch'>
        <View style={ApplicationStyles.accListWrapper}>
          <FlatList
            style={{ flex: 0.5 }}
            data={this.state.txns.filtered('amount < 0').sorted('created', true)}
            renderItem={this.renderItem.bind(this)}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            ListFooterComponent={() => (
              this.state.txns.filtered('amount < 0').length > 0 ?
              <View style={ApplicationStyles.separator} /> :
              null
            )}
          />
          <FlatList
            style={{ flex: 0.5 }}
            data={this.state.txns.filtered('amount > 0').sorted('created', true)}
            renderItem={this.renderItem.bind(this)}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            ListFooterComponent={() => (
              this.state.txns.filtered('amount > 0').length > 0 ?
              <View style={ApplicationStyles.separator} /> :
              null
            )}
          />
          </View>
          <View style={ApplicationStyles.separator} />
          <Text style={ApplicationStyles.balance}>
            <Text style={{ color: Colors.black }}>{I18n.t('balance')}</Text>
            <Text
              style={
              this.state.balance >= 0 ?
              { color: Colors.black } : { color: Colors.fire }}
            >
              {accounting.formatMoney(Math.abs(this.state.balance), '₹', 0)}
            </Text>
          </Text>
      </Image>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    resizeMode: 'stretch'
  }
});
