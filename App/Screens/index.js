import { Navigation } from 'react-native-navigation';

import Cash from './Cash';
import Account from './Account';
import Playground from './Playground';
import New from './New';
import AccountDetail from './AccountDetail';
import EditName from './EditName';
import AddAccount from './AddAccount';
import EditAccount from './EditAccount';
import Setting from './Setting';
import AttachDetail from './AttachDetail';
import Login from './Login';
import Drawer from './Drawer';
import Grow from './Grow';

export function registerScreens() {
  Navigation.registerComponent('screen.cash', () => Cash);
  Navigation.registerComponent('screen.account', () => Account);
  Navigation.registerComponent('screen.new', () => New);
  Navigation.registerComponent('screen.details', () => AccountDetail);
  Navigation.registerComponent('screen.editName', () => EditName);
  Navigation.registerComponent('screen.editAccount', () => EditAccount);
  Navigation.registerComponent('screen.addAccount', () => AddAccount);
  Navigation.registerComponent('screen.Drawer', () => Drawer);
  Navigation.registerComponent('screen.playground', () => Playground);
  Navigation.registerComponent('screen.setting', () => Setting);
  Navigation.registerComponent('screen.attachDetail', () => AttachDetail);
  Navigation.registerComponent('screen.login', () => Login);
  Navigation.registerComponent('screen.grow', () => Grow);
}
