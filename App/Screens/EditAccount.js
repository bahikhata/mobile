import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Button, TextInput, ScrollView, ToastAndroid } from 'react-native';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const _ = require('lodash');
const I18n = require('../I18n/I18n');
const realm = require('../Services/dbClient');
const validate = require('validate.js');

const constraints = {
  account: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class EditAccount extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      accountError: ''
    };
    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });
    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
  }

  componentWillMount() {
    const { id } = this.props;

    this.props.navigator.setTitle({
      title: I18n.t('editAcc')
    });

    const account = realm.objects('Account').filtered('id = $0', id);
    this.setState({
      account: account[0].name
    });
  }

  componentWillUnmount() {
      this.onPressButtonDebounced.cancel();
  }

  onPressButton = () => {
    const error = validate({ account: this.state.account }, constraints);
    if (!error) {
      const account = realm.objects('Account').filtered('name = $0', this.state.account);
      if (account.length === 0) {
        realm.write(() => {
          realm.create('Account', {
            id: this.props.id,
            name: this.state.account,
            updated: new Date()
          }, true);
        });
        this.props.navigator.pop({
          animated: true,
          animationType: 'slide-horizontal'
        });
      } else {
        this.setState({
          accountError: I18n.t('accountExists')
        });
      }
    } else {
      this.setState({
        accountError: error.account[0]
      });
    }
  }

  render() {
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
          <TextInput
            style={ApplicationStyles.inputControl}
              placeholder={I18n.t('account')}
              ref='account'
              value={this.state.account}
              maxLength={35}
              autoFocus
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ account: text, accountError: '' })}
          />
          {
            this.state.accountError ?
            <Text style={ApplicationStyles.inputError}>
              {this.state.accountError}
            </Text> : null
          }
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
