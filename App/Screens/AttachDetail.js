import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image, Text, ImageStore, ImageEditor } from 'react-native';
import { iconsMap, iconsLoaded } from '../app-icons';
import Share from 'react-native-share';
import { ApplicationStyles, Colors, Metrics, Images, Fonts } from '../Themes';

const I18n = require('../I18n/I18n');

export default class AttachDetail extends Component {
  constructor(props) {
    super(props);

    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });

    iconsLoaded.then(() => {
      this.props.navigator.setButtons({
        rightButtons: [
        {
          icon: iconsMap.share,
          id: 'share',
          buttonColor: Colors.snow
        }],
        animated: false
        });
    });

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    if (event.id === 'share') {
      const imageURL = this.props.txn.attach;
      console.log(imageURL);
      Image.getSize(imageURL, (width, height) => {
        const imageSize = {
          size: { width, height },
          offset: { x: 0, y: 0 }
        };
        ImageEditor.cropImage(imageURL, imageSize, (imageURI) => {
            ImageStore.getBase64ForTag(imageURI, (base64Data) => {
              //console.log(base64Data);
              Share.open({
                message: I18n.t('shareMessage'),
                title: I18n.t('shareTitle'),
                url: `data:image/png;base64,${base64Data}`,
                type: 'image/png'
              });
              ImageStore.removeImageForTag(imageURI);
            }, (reason) => console.log(reason));
          }, (reason) => console.log(reason));
        }, (reason) => console.log(reason));
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{ flex: 0.9, resizeMode: 'contain' }}
          source={{ uri: this.props.txn.attach }}
        />
        <View
          style={{ flex: 0.1 }}
        >
          {
            this.props.txn.account ?
              <Text
                style={[styles.text, { fontWeight: 'bold' }]}
              >
                {this.props.txn.account.name}
              </Text>
              : null
          }
          <Text style={styles.text}>
            {this.props.txn.detail}
          </Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black
  },
  button: {
    margin: 50,
    padding: 20
  },
  inputControl: {
    margin: 10
  },
  text: {
    color: Colors.snow,
    textAlign: 'center'
  }
});
