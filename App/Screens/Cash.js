import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, View, Image,
  DatePickerAndroid, Alert,
  TouchableHighlight, FlatList
} from 'react-native';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import { iconsMap, iconsLoaded } from '../app-icons';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import * as Animatable from 'react-native-animatable';
import { Colors, Metrics, Images, ApplicationStyles, Fonts } from '../Themes';

const _ = require('lodash');
const SharedPreferences = require('react-native-shared-preferences');
const realm = require('../Services/dbClient');
const accounting = require('accounting');
const I18n = require('../I18n/I18n');

export default class Cash extends Component {
  static navigatorStyle = {
    contextualMenuStatusBarColor: Colors.primary,
    contextualMenuBackgroundColor: Colors.primary,
    contextualMenuButtonsColor: Colors.snow
  };

  constructor(props) {
    super(props);
    this.state = {
      txns: [],
      date: new Date(),
      headerVisible: true,
      selected: -1
    };

    iconsLoaded.then(() => {
      this.props.navigator.setButtons({
        leftButtons: [{
          id: 'sideMenu',
          buttonColor: Colors.snow
        }],
        fab: {
          collapsedId: 'new',
          collapsedIcon: iconsMap.pencil,
          backgroundColor: Colors.darkPrimary
        },
        animated: true
      });
    });

    global.navigator = this.props.navigator;

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    this.newTxnDebounced = _.debounce(this.newTxn, 300);
    this.viewAccountDebounced = _.debounce(this.viewAccount, 300);
    this.deleteTxnDebounced = _.debounce(this.deleteTxn, 300);
  }

  componentWillMount() {
    this.props.navigator.toggleNavBar({
        to: 'hidden', // required, 'hidden' = hide navigation bar, 'shown' = show navigation bar
        animated: false
    });
    this.updateTxns();

    SharedPreferences.getItem('firstTime', (value) => {
      if (value === null) {
        this.props.navigator.showSnackbar({
          text: `${I18n.t('welcome')}  ${realm.objects('User')[0].name}`,
          duration: 'long'
        });
        SharedPreferences.setItem('firstTime', 'false');
      }
    });
  }

  componentWillUnmount() {
    this.newTxnDebounced.cancel();
    this.viewAccountDebounced.cancel();
    this.deleteTxnDebounced.cancel();
  }

  onNavigatorEvent = (event) => {
    if (event.id === 'new') {
      this.newTxnDebounced();
    } else if (event.id === 'willAppear') {
      this.forceUpdate();
    } else if (event.id === 'bottomTabSelected' || event.id === 'bottomTabReselected') {
      this.props.navigator.dismissContextualMenu();
    } else if (event.id === 'contextualMenuDismissed') {
      this.setState({
        headerVisible: true,
        selected: -1
      });
      this.props.navigator.toggleNavBar({
          to: 'hidden', // required, 'hidden' = hide navigation bar, 'shown' = show navigation bar
          animated: false
      });
    }
  }

  onSwipeLeft = (gestureState) => {
    const today = new Date();
    const newDate = moment(this.state.date).add(1, 'days').toDate();
    if (newDate <= today) {
      this.setState({
        date: newDate
      }, function () {
        this.updateTxns();
      });
    }
    this.refs.txns.bounceInRight();
  }

  onSwipeRight = (gestureState) => {
    const newDate = moment(this.state.date).subtract(1, 'days').toDate();
    this.setState({
        date: newDate
    }, function () {
        this.updateTxns();
        this.refs.txns.bounceInLeft();
    });
  }

  newTxn = () => {
    this.props.navigator.dismissContextualMenu();
    this.props.navigator.push({
      screen: 'screen.new', // unique ID registered with Navigation.registerScreen
      title: I18n.t('newTxn'), // navigation bar title of the pushed screen (optional)
      passProps: {}, // Object that will be passed as props to the pushed screen (optional)
      animated: true,
      animationType: 'slide-horizontal',
      backButtonHidden: false,
    });
  }

  toggleDrawer = () => {
    this.props.navigator.toggleDrawer({
      side: 'left',
      animated: true
    });
  };

  showPicker = async(options) => {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open(options);
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        this.setState({
          date: new Date(year, month, day)
        });
        this.updateTxns();
      }
    } catch ({ code, message }) {
      console.warn('Error in example ', message);
    }
  };

  updateTxns = () => {
    const date = this.state.date;
    const firstDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
    const nextDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
    this.setState({
      txns: realm.objects('Transaction')
        .filtered('created >= $0 AND created < $1', firstDate, nextDate)
    });
  }

  cash = (type) => {
    const date = this.state.date;
    const firstDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
    const lastDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
    if (type === 1) {
      return realm.objects('Transaction')
      .filtered('created <= $0 AND cancelled = false', firstDate).sum('amount');
    }
      return realm.objects('Transaction')
      .filtered('created <= $0 AND cancelled = false', lastDate).sum('amount');
  }

  openBalance = () => (
      <View>
        <View style={styles.group}>
          <Text style={styles.openAmount}>
            {accounting.formatMoney(Math.abs(this.cash(1)), '₹', 0)}
          </Text>
          <Text style={styles.openDetail}>{I18n.t('openBalance')}</Text>
        </View>
        <View style={ApplicationStyles.separator} />
      </View>
  )

  closeBalance = () => (
      <View>
        {this.state.txns.filtered('amount < 0').length > 0 ?
        <View style={ApplicationStyles.separator} /> : null}
        <View style={styles.group}>
          <Text style={styles.openAmount}>
            {accounting.formatMoney(Math.abs(this.cash(-1)), '₹', 0)}
          </Text>
          <Text style={styles.openDetail}>{I18n.t('closeBalance')}</Text>
        </View>
        <View style={ApplicationStyles.separator} />
      </View>
  )

  longPress = (rowId) => {
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: false
    });

    this.setState({
      headerVisible: false
    });

    const buttons = [{
      title: I18n.t('delete'),
      icon: iconsMap.trash
    }];

    const txn = realm.objects('Transaction').filtered('id = $0', rowId);
    if (txn[0].account != null) {
      buttons.push({
        title: I18n.t('view'),
        icon: iconsMap.user
      });
    }

    //Visually show the txn selected
    this.setState({
      selected: rowId
    });

    //Show context menu
    this.props.navigator.showContextualMenu(
      {
        rightButtons: buttons,
        onButtonPressed: (index) => {
          if (index === 0) {
            Alert.alert(
              '',
              I18n.t('deleteTxnMsg'),
              [
                { text: I18n.t('cancel') },
                { text: I18n.t('ok'), onPress: () => this.deleteTxnDebounced(txn[0]) }
              ],
              { cancelable: true }
            );
          } else if (index === 1) {
            this.viewAccountDebounced(txn[0]);
          }
          this.setState({
            headerVisible: true
          });
          this.props.navigator.toggleNavBar({
            to: 'hidden',
            animated: false
          });
        }
      }
    );
  }

  viewAttachDetail = (rowId) => {
    const txn = realm.objects('Transaction').filtered('id = $0', rowId);
    //alert(txn[0].attach);

    this.props.navigator.push({
      screen: 'screen.attachDetail',
      passProps: {
        txn: txn[0]
      },
      animated: true,
      animationType: 'fade',
      backButtonHidden: false,
      navigatorStyle: {
        navBarBackgroundColor: Colors.black,
        navBarTextColor: Colors.snow,
        statusBarColor: Colors.black
      }
    });
  }

  viewAccount = (txn) => {
    this.props.navigator.push({
      screen: 'screen.details',
      title: '',
      passProps: {
        id: txn.account.id
      },
      animated: true,
      animationType: 'slide-horizontal',
      backButtonHidden: false,
    });
  }

  deleteTxn = (txn) => {
    if (txn.account != null) {
      realm.write(() => {
        realm.create('Account', {
          id: txn.account.id,
          amount: txn.account.amount - txn.amount,
          updated: new Date()
        }, true);
        realm.delete(txn);
      });
    } else {
      realm.write(() => {
        realm.delete(txn);
      });
    }
    this.forceUpdate();
  }

  renderItem = (rowData) => {
    let account = null;
    let detail = null;
    let attach = null;

    if (rowData.item.account) {
      account = <Text style={ApplicationStyles.account}>{rowData.item.account.name}</Text>;
    }
    if (rowData.item.detail) {
      detail = <Text style={ApplicationStyles.detail}>{rowData.item.detail}</Text>;
    }
    if (rowData.item.attach) {
      attach = (
        <TouchableHighlight
          underlayColor={Colors.underlayColor}
          onPress={() => this.viewAttachDetail(rowData.item.id)}
        >
          <Image
            style={ApplicationStyles.attach} source={{ uri: rowData.item.attach }}
          />
        </TouchableHighlight>);
    }
    return (
      <TouchableHighlight
        underlayColor={Colors.underlayColor}
        onLongPress={() => this.longPress(rowData.item.id)}
      >
        <View
          style={[ApplicationStyles.row,
            this.state.selected === rowData.item.id ?
            { backgroundColor: Colors.lightPrimary } :
            { backgroundColor: 'transparent' }]}
        >
          <Text style={ApplicationStyles.txnAmount}>
            {accounting.formatMoney(Math.abs(rowData.item.amount), '₹', 0)}
          </Text>
          <View style={ApplicationStyles.subrow}>
              {account}
              {detail}
              {attach}
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  renderSeparator = () => (
      <View style={ApplicationStyles.separator} />
  )

  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    return (
      <GestureRecognizer
        onSwipeLeft={(state) => this.onSwipeLeft(state)}
        onSwipeRight={(state) => this.onSwipeRight(state)}
        config={config}
        style={{ flex: 1 }}
      >
        <View style={ApplicationStyles.container}>
          <View
            style={this.state.headerVisible ?
            styles.dateContainer : styles.dateContainerHidden} ref="header"
          >
            <Icon
                name='bars'
                size={Metrics.icons.small}
                color={Colors.snow}
                style={styles.barsMenu}
                onPress={this.toggleDrawer}
            />
            <View style={styles.dateCalGroup}>
              <Icon
                  name='calendar'
                  size={Metrics.icons.small}
                  color={Colors.snow}
                  style={styles.calendarMenu}
                  onPress={() => this.showPicker({ date: this.state.date, maxDate: new Date() })}
              />
              <Text
                style={styles.date}
                onPress={() => this.showPicker({ date: this.state.date, maxDate: new Date() })}
              >
              {moment(this.state.date).format('DD MMM')}
              </Text>
            </View>
          </View>
          <Animatable.Image
            style={styles.container}
            ref="txns" source={Images.background}
          >
                <FlatList
                    style={{ flex: 0.5 }}
                    data={this.state.txns.filtered('amount > 0').sorted('created', false)}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.id}
                    ItemSeparatorComponent={this.renderSeparator}
                    ListHeaderComponent={this.openBalance}
                    ListFooterComponent={() => (
                      this.state.txns.filtered('amount > 0').length > 0 ?
                        <View style={ApplicationStyles.separator} /> : null
                    )}
                />
                <FlatList
                    style={{ flex: 0.5 }}
                    data={this.state.txns.filtered('amount < 0').sorted('created', false)}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.id}
                    ItemSeparatorComponent={this.renderSeparator}
                    ListFooterComponent={this.closeBalance}
                />

          </Animatable.Image>
      </View>
      </GestureRecognizer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.89,
    flexDirection: 'row',
    width: '100%',
    resizeMode: 'stretch'
  },
  openAmount: {
    flex: 0.27,
    color: Colors.charcoal,
    fontSize: Fonts.size.small,
    fontStyle: 'italic',
    textAlignVertical: 'center',
    textAlign: 'right'
  },
  openDetail: {
    flex: 0.73,
    color: Colors.black,
    fontSize: Fonts.size.medium,
    textAlignVertical: 'center',
    fontWeight: 'bold',
    marginLeft: Metrics.smallMargin
  },
  group: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2
  },
  barsMenu: {
    flex: 0.1,
    textAlignVertical: 'center',
    margin: Metrics.doubleBaseMargin - 2
  },
  calendarMenu: {
    flex: 0.1,
    textAlignVertical: 'center'
  },
  date: {
    flex: 0.5,
    fontSize: Fonts.size.h4,
    color: Colors.snow,
    justifyContent: 'center',
    marginRight: Metrics.doubleBaseMargin,
    textAlign: 'center',
    alignSelf: 'center',
    fontStyle: 'italic',
    textAlignVertical: 'center'
  },
  dateCalGroup: {
    flex: 0.7,
    flexDirection: 'row',
    marginLeft: Metrics.doubleBaseMargin,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  dateContainer: {
    flex: 0.11,
    flexDirection: 'row',
    backgroundColor: Colors.primary,
    shadowColor: Colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4
  },
  dateContainerHidden: {
    display: 'none'
  }
});
