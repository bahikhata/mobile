import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, TextInput, ScrollView, StyleSheet, Button, Image
} from 'react-native';
import RNRestart from 'react-native-restart';
import { ApplicationStyles, Colors, Metrics, Images } from '../Themes';

const _ = require('lodash');
const validate = require('validate.js');
const I18n = require('../I18n/I18n');
const realm = require('../Services/dbClient');

const constraints = {
  name: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class Login extends Component {
  static navigatorStyle = {
    navBarTextColor: Colors.snow,
    navBarComponentAlignment: 'center',
    screenBackgroundColor: Colors.snow,
    navBarBackgroundColor: Colors.primary,
  };
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      nameError: ''
    };

    this.props.navigator.setTitle({
      title: I18n.t('app_name')
    });

    this.props.navigator.setDrawerEnabled({
      side: 'left',
      enabled: false
    });

    this.startAppDebounced = _.debounce(this.startApp, 300);
  }

  componentDidMount() {
    //alert(I18n.currentLocale());
  }

  componentWillUnmount() {
    this.startAppDebounced.cancel();
  }

  startApp = () => {
    const error = validate({ name: this.state.name }, constraints);
    if (!error) {
      realm.write(() => {
        realm.create('User', {
          id: 1,
          name: this.state.name,
          phone: ''
        });
      });

      //If previous account exists then sync the previous data else show main screen.
      RNRestart.Restart();
    } else {
      this.setState({
        nameError: error.name[0]
      });
    }
  }

  render() {
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
        <View style={styles.view}>
          <TextInput
              style={ApplicationStyles.inputControl}
              placeholder={I18n.t('business')}
              multiline={false}
              autoCorrect={false}
              maxLength={25}
              value={this.state.name}
              selectionColor={Colors.black}
              placeholderTextColor={Colors.charcoal}
              underlineColorAndroid={Colors.transparent}
              onChangeText={(text) => this.setState({ name: text, nameError: '' })}
          />
          {
            this.state.nameError ?
            <Text style={ApplicationStyles.inputError}>{this.state.nameError}</Text>
            : null
          }
        </View>
        <View style={styles.view}>
          <Button
            title={I18n.t('start')}
            color={Colors.darkPrimary}
            onPress={this.startAppDebounced}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  imageView: {
    margin: Metrics.doubleSection,
    alignItems: 'center',
    alignContent: 'center'
  },
  image: {
    width: Metrics.images.placeholder,
    height: Metrics.images.placeholder,
    resizeMode: 'contain'
  },
  view: {
    margin: Metrics.baseMargin
  }
});
