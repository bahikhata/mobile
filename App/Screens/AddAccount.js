import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Button,
  TextInput, ScrollView, ToastAndroid }
  from 'react-native';
import SelectContacts from 'react-native-select-contact-android';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Colors, Metrics, Images, ApplicationStyles } from '../Themes';

const _ = require('lodash');
const I18n = require('../I18n/I18n');
const realm = require('../Services/dbClient');
const validate = require('validate.js');

const constraints = {
  account: {
    presence: {
      message: I18n.t('required')
    }
  }
};

export default class AddAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      accountError: '',
      phone: ''
    };
    this.props.navigator.toggleTabs({
      to: 'hidden',
      animated: false
    });
    this.onPressButtonDebounced = _.debounce(this.onPressButton, 300);
    this.pickContactDebounced = _.debounce(this.pickContact, 300);
  }

  componentWillMount() {

  }

  componentWillUnmount() {
    this.onPressButtonDebounced.cancel();
    this.pickContactDebounced.cancel();
  }

  onPressButton = () => {
    const error = validate({ account: this.state.account }, constraints);
    if (!error) {
      const account = realm.objects('Account').filtered('name = $0', this.state.account);
      if (account.length === 0) {
        realm.write(() => {
          realm.create('Account', {
            id: (realm.objects('Account').length > 0 ?
                realm.objects('Account').max('id') : 0) + 1,
            name: this.state.account,
            amount: 0,
            phone: this.state.phone
          });
        });
        this.props.navigator.pop({
          animated: true,
          animationType: 'slide-horizontal'
        });
      } else {
        this.setState({
          accountError: I18n.t('accountExists')
        });
      }
    } else {
      this.setState({
        accountError: error.account ? error.account[0] : ''
      });
    }
  }

  pickContact = () => {
    SelectContacts.pickContact({ timeout: 45000 }, (err, contact) => {
      if (err) {
        console.log(err.message);
      } else {
        //Check if contact does not exist of same name.
        const account = realm.objects('Account').filtered('name = $0', contact.name);
        if (account.length === 0) {
          this.setState({
            account: contact.name,
            phone: contact.phoneNumbers ? contact.phoneNumbers[0].number : ''
          });
        } else {
          this.setState({
            accountError: I18n.t('accountExists')
          });
        }
      }
    });
  }

  render() {
    return (
      <ScrollView style={ApplicationStyles.container} keyboardShouldPersistTaps='always'>
          <View style={ApplicationStyles.miniForm}>
            <TextInput
              style={ApplicationStyles.inputControl}
                placeholder={I18n.t('account')}
                ref='account'
                value={this.state.account}
                maxLength={35}
                autoFocus
                selectionColor={Colors.black}
                placeholderTextColor={Colors.charcoal}
                underlineColorAndroid={Colors.transparent}
                onChangeText={(text) => this.setState({ account: text, accountError: '' })}
            />
            <Icon
              name='user'
              size={Metrics.icons.medium}
              color={Colors.black}
              style={ApplicationStyles.contactIcon}
              onPress={this.pickContactDebounced}
            />
          </View>
          {
            this.state.accountError ?
            <Text style={ApplicationStyles.inputError}>
              {this.state.accountError}
            </Text> : null
          }
          <View style={ApplicationStyles.buttonWrapper}>
              <Button
                title={I18n.t('ok')}
                color={Colors.primary}
                onPress={this.onPressButtonDebounced}
              />
          </View>
      </ScrollView>
    );
  }
}
