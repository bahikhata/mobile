import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import React, { Component } from 'react';
import codePush from 'react-native-code-push';
import { registerScreens } from './Screens';
import { iconsMap, iconsLoaded } from './app-icons';
import { Colors, Fonts } from './Themes';

const I18n = require('./I18n/I18n');
const realm = require('./Services/dbClient');

registerScreens();

class App extends Component {
  constructor(props) {
    super(props);
    iconsLoaded.then(() => {
      //codePush.sync();
      this.startApp();
    });
  }
  componentDidMount() {

  }
  startApp = () => {
    const tabs = [{
        label: I18n.t('cash'),
        screen: 'screen.cash',
        icon: iconsMap.money,
        title: I18n.t('cash')
      }, {
        label: I18n.t('accounts'),
        screen: 'screen.account',
        icon: iconsMap.users,
        title: I18n.t('accounts')
      },
      {
        label: I18n.t('grow'),
        screen: 'screen.grow',
        icon: iconsMap['line-chart'],
        title: I18n.t('grow')
      }];
    const loginTabs = [{
        label: I18n.t('cash'),
        screen: 'screen.login',
        icon: iconsMap.money,
        title: I18n.t('cash')
      }, {
        label: I18n.t('accounts'),
        screen: 'screen.account',
        icon: iconsMap.users,
        title: I18n.t('accounts')
      },
      {
        label: I18n.t('grow'),
        screen: 'screen.grow',
        icon: iconsMap['line-chart'],
        title: I18n.t('grow')
      }];

    const appStyle = {
      tabBarBackgroundColor: Colors.primary,
      navBarButtonColor: Colors.snow,
      tabBarButtonColor: Colors.snow,
      navBarTextColor: Colors.snow,
      tabBarSelectedButtonColor: Colors.black,
      navigationBarColor: Colors.primary,
      navBarBackgroundColor: Colors.primary,
      statusBarColor: Colors.primary,
      tabFontFamily: Fonts.type.tab
    };
    const tabsStyle = {
      tabBarBackgroundColor: Colors.primary,
      tabBarButtonColor: Colors.primary,
      tabBarSelectedButtonColor: Colors.lightPrimary,
      tabFontFamily: Fonts.type.tab,
    };
    const user = realm.objects('User');

    if (user.length === 0) {
      Navigation.startSingleScreenApp({
        screen: {
          screen: 'screen.login',
          title: I18n.t('app_name'),
          navigatorStyle: {},
          navigatorButtons: {}
        },
        animationType: 'fade'
      });
    } else {
        Navigation.startTabBasedApp({
          tabs,
          animationType: 'fade',
          tabsStyle,
          appStyle,
          drawer: {
            left: {
              screen: 'screen.Drawer'
            }
          }
        });
      }
  }
}

//const codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };
//codePush
export default App;
