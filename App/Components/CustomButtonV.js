import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TouchableHighlight, Text, View, StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts } from '../Themes';

export default class CustomButton extends Component {
  static propTypes = {
    text: PropTypes.string,
    iconName: PropTypes.string,
    onPress: PropTypes.func,
  }

  render() {
    return (
      <TouchableHighlight
        style={styles.menuItem}
        onPress={this.props.onPress}
        underlayColor={Colors.underlayColor}
      >
        <View style={{ alignItems: 'center' }}>
          <Icon
              name={this.props.iconName}
              size={Metrics.icons.medium}
              color={Colors.snow}
              style={styles.menuButton}
          />
          <Text style={styles.menuText}>{this.props.text && this.props.text.toUpperCase()}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  menuItem: {
    flex: 0.333,
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: Metrics.marginHorizontal,
    paddingTop: Metrics.marginHorizontal
  },
  menuButton: {
    paddingBottom: Metrics.marginHorizontal
  },
  menuText: {
    color: Colors.snow,
    fontSize: Fonts.size.regular
  }
});
