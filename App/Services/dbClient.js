const Realm = require('realm');

const TransactionSchema = {
  name: 'Transaction',
  primaryKey: 'id',
  properties: {
    id: 'int',
    account: { type: 'Account' },
    amount: 'double',
    detail: 'string',
    attach: 'string',
    cancelled: { type: 'bool', default: false },
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

const AccountSchema = {
  name: 'Account',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    amount: 'double',
    phone: 'string',
    cancelled: { type: 'bool', default: false },
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

const UserSchema = {
  name: 'User',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    phone: 'string',
    created: { type: 'date', default: new Date() },
    updated: { type: 'date', default: new Date() }
  }
};

/*Realm.Sync.User.login('http://0.0.0.0:9080', 'realm-admin', '').then(user => {
  // user is logged in
  // do stuff ...
  alert("Logged In" + user);
}).catch(error => {
  // an auth error has occurred
  alert("Error"+ error);
});*/
/*Realm.Sync.User.register('http://192.168.0.101:9080','abhi','fundoo0', (error,user) => {
  if(!error){

  }
  else{

  }
});
Realm.Sync.User.login('http://192.168.0.101:9080','abhi','fundoo0').then(user => {
  // user is logged in
  // do stuff ...
  console.log(Realm.Sync.User.All);

  console.log(user);
}).catch(error => {
  // an auth error has occurred
  alert("Error"+ error);
});


const config = {
  sync: {
    user: Realm.Sync.User.current,
    url: 'realm://192.168.0.101:9080/~/bahi-realm',
    error: err => console.log(err)
  },
  schema : [TransactionSchema,AccountSchema,UserSchema],
  schemaVersion: 1
}
/*
const config = {
  schema: [TransactionSchema,AccountSchema,UserSchema],
  schemaVersion: 1
};

let realm = new Realm(config);
const progressCallback = (transferred,transferables) => {
  if(transferred < transferables){
    console.log("Uploading ... ", transferred/transferables);
  } else {
    console.log("Done Uploading ... ");
  }
}

//realm.syncSession.addProgressNotification('upload','reportInfinitely',progressCallback);
realm.syncSession.addProgressNotification('download',
'forCurrentlyOutstandingWork',progressCallback);
realm.syncSession.removeProgressNotification(progressCallback);
*/

const config = {
  schema: [TransactionSchema, AccountSchema, UserSchema],
  schemaVersion: 1
};

const realm = new Realm(config);

module.exports = realm;
